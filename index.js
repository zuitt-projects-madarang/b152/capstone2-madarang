const express = require("express")
const mongoose = require("mongoose")
const app = express()
const port = 4000

mongoose.connect("mongodb+srv://aidz113:Hesoyam69420@cluster0.tuthz.mongodb.net/eCommerceAPI152?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.on("error",console.error.bind(console, "Connection Error"))
db.once("open",()=>console.log("Connected to MongoDB"))

app.use(express.json())

//User Routes
const userRoutes = require("./routes/userRoutes")
app.use('/users',userRoutes)

//Product Routes
const productRoutes = require("./routes/productRoutes")
app.use('/products',productRoutes)

//Order Routes
const orderRoutes = require("./routes/orderRoutes")
app.use('/orders',orderRoutes)

app.listen(port,()=>console.log(`Server is running at port ${port}.`))