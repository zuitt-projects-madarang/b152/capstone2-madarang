const User = require("../models/User") 

const bcrypt = require("bcrypt")

const auth = require("../auth")

//User Registration
module.exports.register = (req,res) => {
	//console.log(req.body)
	const hashedPw = bcrypt.hashSync(req.body.password,10)

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo
	})
	//console.log(newUser)
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err))
}

module.exports.login = (req,res) => {
	//console.log(req.body)
	User.findOne({email: req.body.email})
	.then(foundUser => {
		if (foundUser === null){
			return res.send("No user found.")
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password)
			//console.log(isPasswordCorrect)
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send("Incorrect password.")
			}
		}
	})
	.catch(err => res.send(err))
}

module.exports.updateAdmin = (req,res) => {
	//console.log(req.params)
	let updates = {
		isAdmin: true
	}
	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(adminUser => res.send(adminUser))
	.catch(err => res.send(err))
}
