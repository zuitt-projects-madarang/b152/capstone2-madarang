const Order = require("../models/Order")

const Product = require('../models/Product')

module.exports.createOrder = async (req,res) => {
	if (req.user.isAdmin){
		return res.send("Action Forbidden.")
	}

	let newOrder = new Order({
		userId: req.user.id,
		totalAmount: req.body.totalAmount,
		products: req.body.products
	})
	//console.log(newOrder)
	let orderId = newOrder._id
	//console.log(orderId)
	newOrder.products.forEach((product) => {
		//console.log(product)
		Product.findById(product.productId)
		.then(result => {
			let orderProduct = {
				orderId: orderId,
				quantity: product.quantity
			}
			result.orders.push(orderProduct)
			return result.save()
			.then(result => true)
			.catch(err => res.send(err))
		})
	})
	newOrder.save()
	.then(order => res.send("Order successful."))
	.catch(err => res.send(err))
}

module.exports.getUserOrders = (req,res) => {
	Order.find({userId:req.user.id})
	.then(foundOrder => res.send(foundOrder))
	.catch(err => res.send(err))
}

module.exports.getAllOrders = (req,res) => {
	Order.find({})
	.then(orders => res.send(orders))
	.catch(err => res.send(err))
}