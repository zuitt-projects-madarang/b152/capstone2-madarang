 const Product = require("../models/Product")

 module.exports.createProduct = (req,res) => {
 	//console.log(req.body)
 	let newProduct = new Product({
 		name: req.body.name,
 		description: req.body.description,
 		price: req.body.price
 	})
 	//console.log(newProduct)
 	newProduct.save()
 	.then(product => res.send(product))
 	.catch(err => res.send(err))
 }

 module.exports.getActiveProducts = (req,res) => {
 	Product.find({isActive: true})
 	.then(activeProducts => res.send(activeProducts))
 	.catch(err => res.send(err))
 }

 module.exports.getSingleProduct = (req,res) => {
 	Product.findById(req.params.id)
 	.then(product => res.send(product))
 	.catch(err => res.send(err))
 }

 module.exports.updateProduct = (req,res) => {
    let updates = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }
    Product.findByIdAndUpdate(req.params.id,updates,{new:true})
    .then(updatedProduct => res.send(updatedProduct))
    .catch(err => res.send(err))
 }

 module.exports.archiveProduct = (req,res) => {
    let updates = {
        isActive: false
    }
    Product.findByIdAndUpdate(req.params.id,updates,{new:true})
    .then(archivedProduct => res.send(archivedProduct))
    .catch(err => res.send(err))
 }