const express = require("express")
const router = express.Router()

const userControllers = require("../controllers/userControllers")

const auth = require("../auth")
const {verify,verifyAdmin} = auth

//User Registration
router.post('/',userControllers.register)

//User Authentication
router.post('/login',userControllers.login)

//Update Admin
router.put('/updateAdmin/:id',verify,verifyAdmin,userControllers.updateAdmin)

module.exports = router
