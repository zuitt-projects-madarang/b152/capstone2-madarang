const express = require("express")
const router = express.Router()

const orderControllers = require("../controllers/orderControllers")

const auth = require("../auth")
const {verify,verifyAdmin} = auth

//Create Order
router.post('/',verify,orderControllers.createOrder)

//Get User's Orders
router.get('/getUserOrders',verify,orderControllers.getUserOrders)

//Get All Orders
router.get('/',verify,verifyAdmin,orderControllers.getAllOrders)

module.exports = router