const express = require("express")
const router = express.Router()

const productControllers = require("../controllers/productControllers")

const auth = require("../auth")
const {verify,verifyAdmin} = auth

//Create Product
router.post('/',verify,verifyAdmin,productControllers.createProduct)

//Get Active Products
router.get('/active',productControllers.getActiveProducts)

//Get Single Product
router.get('/getSingleProduct/:id',productControllers.getSingleProduct)

//Update Product
router.put('/:id',verify,verifyAdmin,productControllers.updateProduct)

//Archive Product
router.put('/archive/:id',verify,verifyAdmin,productControllers.archiveProduct)

module.exports = router